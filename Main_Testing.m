
% Bryce Mack

% Main Testing for Mulitple Nodes in Hidden  Layer
ibias = 3;
z1 = [ones(1,ibias) 0 0;
      ones(1,ibias) 0 1;
      ones(1,ibias) 1 0;
      ones(1,ibias) 1 1];

for n = 1:4
    a1 = z1(n,:);
    a2 = forwardProp(theta1,a1');
    a3 = theta2*[1;a2];
    output = activationFun(-a3);
    fprintf('Output = %f\n', output)
end