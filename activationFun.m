
% Bryce_Mack

% Activation Function

function [z] = activationFun(x)
    z = 1./(1+exp(x));
end