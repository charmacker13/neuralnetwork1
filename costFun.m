
% Bryce_Mack

% Calculate cost function

function [J] = costFun(y,h)
    J = ((y * log(h)) + ((1 - y) * log(1 - h))) ;
end