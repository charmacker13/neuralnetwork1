
% Bryce_Mack

% Main NN
%%
%{
First lets clear our workspace and close all windows.  
Note: generally this is not recommended unless you plan to run only your 
program in isolation.
%}
clear
close all
clc
%% Define hyperparameters for our network
LearningRate = 0.1;
noEpochs = 100001;
plotRate = 101; % How often to test and display the network performance
%%=========================================================================

%% Test Data set - self explanatory
inputDataAND = [0 0 0
                0 1 0
                1 0 0
                1 1 1];

inputDataXOR = [0 0 0
                0 1 1
                1 0 1
                1 1 0];

inputDataOR =  [0 0 0
                0 1 1
                1 0 1
                1 1 1];
            
% pick one data set for this network            
inputData = inputDataXOR; % assign any one of the data from above
%%=========================================================================
%% weights or parameters
%{
    \begin{equation*}
\theta^1 = 
\begin{bmatrix}
\theta{^1}_{(1,1)} & \theta{^1}_{(1,2)} & \theta{^1}_{(1,3)} \\
\theta{^1}_{(2,1)} & \theta{^1}_{(2,2)} & \theta{^1}_{(2,3)} \\
\end{bmatrix}
\end{equation*}


Remember from lecture: $\theta{^l}_{(i,j)}$ means weight for $\mathit{l}$ connecting node $\mathit{j}$ in layer  $\mathit{l}$ to node  $\mathit{i}$ in layer $\mathit{l+1}$. 
%}
% Assign weights randomly at start but not zeros! why?
%% Biases
ibiasnodes = 3;                                             % bias nodes for input layer
hbiasnodes = 1;                                             % bias nodes per hidden layer
%% Layers
inputNodes = 2+ibiasnodes;                                  % 2 input nodes + x bias nodes
hlayer1Nodes = 5;                                           % hidden layer nodes
%% Output Nodes
outputNodes = 1;                                            % 1 output node
% Assign weights randomly at start but not zeros! why?
theta1 = 2*rand(hlayer1Nodes,inputNodes)-1;                 % weights layer 1 to 2
theta2 = 2*rand(outputNodes,hlayer1Nodes+hbiasnodes)-1;     % weights layer 2 to 3

%%
%{
xt = inputData(1,1:2)';
a1 = [1;xt]; %input with bais value

%% Forward propagation

a2 = forwardProp(theta1,a1);
a3 = forwardProp(theta2,[1;a2]);
h = forwardProp(1,a3);
%}
%% Back propagation
%{
% Calculate error at the output layer
y = inputData(1,3); %expected output
delta3 = h - y;
% error in layer 2
delta2t = ((theta2' * delta3) .* ([1;a2].*(1-[1;a2])));
delta2 = delta2t(2:end);
% adjust weights
theta2 = theta2 - (LearningRate * (delta3 * [1;a2]'));
theta1 = theta1 - (LearningRate * (delta2 * [a1]'));

[theta1_new,theta2_new, J, h, y, delta2, delta3] = MSI_batchNN(inputData,theta1,theta2,LearningRate);

%}

%%=========================================================================

%% Start training the network
saveData = zeros(round(noEpochs/plotRate),hlayer1Nodes+2); %setup matrix to save data
k=1;
% Simulation Starts
clk1 = fix(clock);
fprintf('Started at %2i.%2i.%2i\n\n',clk1(4),clk1(5),clk1(6));
tic
for n=1:noEpochs
    % run one epoch and update the weights 
    [theta1,theta2, J, h, y, delta2, delta3] = MSI_batchNN(inputData,theta1,theta2,LearningRate,ibiasnodes);
    
    % test the network ocassionally and save data
    if(mod(n,plotRate)==0)
        saveData(k,:) = [J delta2' delta3];
        k=k+1;
        %----------------------------------------------------------------------
        % testing
        disp(['Epoch no:' num2str(n)  ', J=' num2str(J)])
        [~,~, ~, h, ~, ~, ~] = MSI_batchNN([0 0 0],theta1,theta2,LearningRate,ibiasnodes);
        disp(['[0 0] ->' num2str(h)]);
        [~,~, ~, h, ~, ~, ~] = MSI_batchNN([0 1 0],theta1,theta2,LearningRate,ibiasnodes);
        disp(['[0 1] ->' num2str(h)]);
        [~,~, ~, h, ~, ~, ~] = MSI_batchNN([1 0 0],theta1,theta2,LearningRate,ibiasnodes);
        disp(['[1 0] ->' num2str(h)]);
        [~,~, ~, h, ~, ~, ~] = MSI_batchNN([1 1 1],theta1,theta2,LearningRate,ibiasnodes);
        disp(['[1 1] ->' num2str(h)]);
    end    
end
%% =========================================================================
% Simulation ends
clk2 = fix(clock);
fprintf('Started at %2i.%2i.%2i\n\n',clk1(4),clk1(5),clk1(6));
fprintf('  ended at %2i.%2i.%2i\n\n', clk2(4),clk2(5),clk2(6));
toc